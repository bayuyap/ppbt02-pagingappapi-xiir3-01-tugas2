package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type user struct {
	ID    int    `bson:"_id" json:"id"`
	Name  string `bson:"name" json:"name"`
	Age   string `bson:"age" json:"age"`
	Email string `bson:"email" json:"email"`
}

func allUsers(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	var newUsers []user
	search := q.Get("s")
	start := q.Get("page")
	limit := q.Get("per_page")
	if search == "" {
		respondWithJSON(w, http.StatusOK, newUsers)
		return
	}
	if start == "" {
		start = "1"
	}
	if limit == "" {
		limit = "100"
	}
	a, _ := strconv.Atoi(start)
	b, _ := strconv.Atoi(limit)
	for i := (a - 1) * b; i < ((a * b) + b); i++ {
		user := user{
			ID:    i,
			Name:  search + "-" + strconv.Itoa(i),
			Age:   "18",
			Email: search + strconv.Itoa(i) + "@gmail.com",
		}
		newUsers = append(newUsers, user)
	}

	convert := map[string][]user{
		"items": newUsers,
	}

	respondWithJSON(w, http.StatusOK, convert)
}

func init() {
	fmt.Println("Initializing Server.....")
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/users", allUsers).Methods("GET")

	originsOk := handlers.AllowedOrigins([]string{"*"})
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Accept"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "OPTIONS"})

	fmt.Println("Server start on Port 8000")
	if err := http.ListenAndServe(":8000", handlers.CORS(originsOk, headersOk, methodsOk)(r)); err != nil {
		log.Fatal(err)
	}
}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJSON(w, code, map[string]string{"error": msg})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}